package plugin

import (
    "bitbucket.org/nsk_tokyo/test"
    "gopkg.in/sensorbee/sensorbee.v0/bql/udf"
)

func init() {
    udf.MustRegisterGlobalUDF("my_test", &udfs.Test{})
}
