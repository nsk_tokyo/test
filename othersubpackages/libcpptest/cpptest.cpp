#include <iostream>

extern "C" {
   void echo();
}

void echo() {
   std::cout << "Hello C++" << std::endl;
}
