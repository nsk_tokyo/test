package udfs

import (
    "gopkg.in/sensorbee/sensorbee.v0/core"
    "gopkg.in/sensorbee/sensorbee.v0/data"
)

/*
#cgo CFLAGS: -I./othersubpackages/libcpptest
#cgo LDFLAGS: -L./othersubpackages/libcpptest -lcpptest
#include "cpptest.h"
*/
import "C"

type Test struct {
}

func (j *Test) Call(ctx *core.Context, args ...data.Value) (data.Value, error) {
    // When my_udf(arg1, arg2) is called, len(args) is 2.
    // args[0] is arg1 and args[1] is arg2.
    // It is guaranteed that m.Accept(len(args)) is always true.
    C.echo();
    return data.String("NSK PLUGIN"), nil
}

func (j *Test) Accept(arity int) bool {
    return true //arity == 2
}

func (j *Test) IsAggregationParameter(k int) bool {
    return false
}

